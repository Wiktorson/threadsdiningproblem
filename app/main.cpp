#include <iostream>
#include <thread>

#include "BurgerEater.hpp"
#include "BurgerTable.hpp"
#include "Chef.hpp"

// Naming could be improved but the aim of this exercise was to get familiar with multithreading

// Exercise summary:
//  Variable number of eaters in the table. Each eater has two forks (left and right one) each fork is shared by 2 eaters.
//  Eater can only consume burger if he has two forks picked up. Eaters are trying to pick up fork and eat burger simultaneously.
//  If eater has one fork and can't pick up another one because it's already taken, he places the fork back on table.

int main()
{
  BurgerTable table(5); // t(crewsize)
  Chef chef;            // initalizing chief

  chef.instantCook(
      table,
      100000); // Immediately place initial n burgers on the table t

  table.eat(); // Give a signal to the table that crew can start eating. Each
               // crew member eats in separate thread

  table.print();

  return 0;
}
