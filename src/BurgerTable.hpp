#ifndef _BurgerTable_HPP_
#define _BurgerTable_HPP_
#include <mutex>
#include <vector>

class BurgerEater;
class Chef;
class Burger;
class Fork;

class BurgerTable {
public:
  explicit BurgerTable(int numberOfPeopleAtTable = 5);
  ~BurgerTable();

  void eat();

  std::vector<Burger>::iterator pickRandomBurger();

  void print() const;
  bool isEmpty() const;

  friend class Chef;
  friend class BurgerEater;

private:
  std::vector<BurgerEater> crew;
  std::vector<Fork> forks;
  int remainingBurgers;
  std::vector<Burger> burgersOnTable;
};
#endif // _BurgerTable_HPP_