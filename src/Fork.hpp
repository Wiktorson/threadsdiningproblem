#ifndef FORK_HPP
#define FORK_HPP

#include <mutex>

class Fork
{
public:
    Fork();

    Fork &operator=(Fork &&source);
    Fork(Fork &&fork);

    bool tryTake();
    void placeBackOnTable();

    friend class BurgerEater;

private:
    std::mutex mtx;
    int id;
    //bool isTaken;
};

#endif