#include <chrono>
#include <thread>
#include <iostream>

#include "Chef.hpp"
#include "BurgerTable.hpp"
#include "Burger.hpp"

int Chef::burgerNum{0};

Chef::Chef(int cookingTime) : burgersCooked(0), cookingTime(cookingTime){};

void Chef::instantCook(BurgerTable &table, int burgerNumber)
{
	for (int i = 1; i <= burgerNumber; ++i)
	{
		int t;
		table.burgersOnTable.emplace_back(Burger(i));
		++burgerNum;
	}
	burgersCooked += burgerNumber;
};
/*
void Chef::cook(BurgerTable *table)
{

	while (table->order > 0)
	{
		std::chrono::milliseconds timespan(cookingTime);
		std::this_thread::sleep_for(timespan);
		table->burgersOnTable.push_front(burgerNum);
		++burgerNum;
		--table->order;
		++burgersCooked;
	}
};*/

void Chef::setCookingTime(int timeMilliSeconds)
{
	cookingTime = timeMilliSeconds;
}