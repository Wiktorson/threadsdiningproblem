#include "BurgerTable.hpp"
#include <iostream>
#include <iterator>
#include <random>
#include <thread>
#include <vector>

#include "Burger.hpp"
#include "BurgerEater.hpp"
#include "Fork.hpp"

BurgerTable::BurgerTable(int numberOfPeopleAtTable)
    : crew(), forks(numberOfPeopleAtTable), remainingBurgers(0) {
  for (int eaterID = 1; eaterID <= numberOfPeopleAtTable; ++eaterID) {
    crew.emplace_back(BurgerEater(eaterID));
  }
  std::cout << "Number of people at the table: " << crew.size() << std::endl;
}

BurgerTable::~BurgerTable() {}

void BurgerTable::print() const {
  for (const auto &e : crew)
    e.print();
}

void BurgerTable::eat() {
  std::vector<std::thread> vecOfThreads;

  for (auto &member :
       crew) // prepare vector of threads with eat function for each crew member
  {
    if (member.getID() < crew.size()) // eaters from 1 to last but one
    {
      vecOfThreads.emplace_back(std::thread(
          &BurgerEater::eat, &member, std::ref(forks.at(member.getID() - 1)),
          std::ref(forks.at(member.getID())), this));
    } else { // last eater in the table
      vecOfThreads.emplace_back(std::thread(
          &BurgerEater::eat, &member, std::ref(forks.at(member.getID() - 1)),
          std::ref(forks.at(0)), this));
    }
  }
  for (auto &th : vecOfThreads) {
    if (th.joinable())
      th.join();
  }
}

bool BurgerTable::isEmpty() const {
  return ((burgersOnTable.size() + remainingBurgers) == 0);
}

std::vector<Burger>::iterator BurgerTable::pickRandomBurger() {
  static std::random_device rd;
  static std::mt19937 gen(rd());
  auto it = burgersOnTable.begin();
  std::uniform_int_distribution<> dis(
      0, std::distance(it, burgersOnTable.end()) - 1);
  std::advance(it, dis(gen));

  return it;
}