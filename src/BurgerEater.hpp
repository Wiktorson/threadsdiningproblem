#ifndef _BURGER_EATER_HPP_
#define _BURGER_EATER_HPP_
#include <vector>

class Fork;
class BurgerTable;
class Burger;

class BurgerEater
{
public:
	BurgerEater(int id);
	~BurgerEater();
	void dupa(Fork &bur);

	void eat(Fork &leftFork, Fork &rightFork, BurgerTable *table); //references to eater's left and right forks from a table perspective
	void pickFork(BurgerTable *table);

	int getID() const;
	void print() const;

private:
	int id;
	std::vector<int> idOfEatenBurgers;
	bool eaterLeftFork;
	bool eaterRightFork;
};

#endif // _BURGER_EATER_HPP_