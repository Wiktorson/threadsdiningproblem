#include "Fork.hpp"
#include <mutex>

Fork &Fork::operator=(Fork &&rhs)
{
  if (this != &rhs)
  { // both rhs and lhs needs to be blocked to prevent other
    // thread accesing it during move operation
    std::unique_lock<std::mutex> lhs_lk(
        mtx, std::defer_lock); // defer_lock is argument to unique lock to do
                               // not take ownership (allows for constuction,
                               // initialization)
    std::unique_lock<std::mutex> rhs_lk(rhs.mtx, std::defer_lock);
    std::lock(lhs_lk, rhs_lk);
    id = std::move(rhs.id);
    //isTaken = std::move(rhs.isTaken);
  }
  return *this;
}

Fork::Fork(Fork &&fork)
{
  std::unique_lock<std::mutex> rhs_lk(fork.mtx);
  //isTaken = std::move(fork.isTaken);
  id = std::move(fork.id);
}

bool Fork::tryTake()
{
  bool locked = mtx.try_lock();
  if (locked) //&& (isTaken == false))
  {
    //isTaken = true;
    //mtx.unlock();
    return true;
  }
  else
  {
    //mtx.unlock();
    return false;
  }
}
Fork::Fork(){}; //isTaken(false) {}

void Fork::placeBackOnTable()
{
  //isTaken = false;
  mtx.unlock();
}