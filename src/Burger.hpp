#ifndef BURGER_HPP
#define BURGER_HPP

#include <mutex>

class Burger
{
public:
  explicit Burger(int id);

  Burger(Burger &&burger);
  Burger &operator=(Burger &&other);
  ~Burger();

  bool tryConsume();

  int getID() const;

private:
  int id;
  bool isConsumed;
  mutable std::mutex mtx;
};

#endif