#ifndef _CHEF_HPP_
#define _CHEF_HPP_

class BurgerTable;
class Chef
{

public:
	explicit Chef(int cookingTime = 100);

	void instantCook(BurgerTable &table, int num);
	void cook(BurgerTable *table);

	void setCookingTime(int timeMilliSeconds);

private:
	static int burgerNum;
	int burgersCooked;
	int cookingTime;
};

#endif // _CHEF_HPP_