#include "BurgerEater.hpp"
#include <chrono>
#include <iostream>
#include <iterator>
#include <mutex>
#include <thread>

#include "Burger.hpp"
#include "BurgerTable.hpp"
#include "Fork.hpp"

std::mutex mtx;

void BurgerEater::dupa(Fork &bur) {}

BurgerEater::BurgerEater(int id)
    : id(id), idOfEatenBurgers(0), eaterLeftFork(false), eaterRightFork(false)
{
}

BurgerEater::~BurgerEater() {}

int BurgerEater::getID() const { return id; }

void BurgerEater::eat(Fork &leftFork, Fork &rightFork, BurgerTable *table)
{
  while (!(table->isEmpty()))
  {
    if (leftFork.tryTake())
    {
      eaterLeftFork = true;
    }
    if (rightFork.tryTake())
    {
      eaterRightFork = true;
    }
    else if (eaterLeftFork == true)
    {
      eaterLeftFork = false;
      leftFork.placeBackOnTable();
    }
    else if (eaterRightFork == true)
    {
      eaterRightFork = false;
      rightFork.placeBackOnTable();
    }
    if ((eaterLeftFork == true) && (eaterRightFork == true) && (table->burgersOnTable.size() > 0))
    {
      auto it = table->burgersOnTable.end(); // table->pickRandomBurger(); //
      if (it->tryConsume())
      {
        idOfEatenBurgers.push_back((it->getID()));
        table->burgersOnTable.erase(it);
      }
      eaterRightFork = false;
      eaterLeftFork = false;
      rightFork.placeBackOnTable();
      leftFork.placeBackOnTable();
      //std::this_thread::sleep_for(std::chrono::nanoseconds(1));
    }
  }
  rightFork.placeBackOnTable();
  leftFork.placeBackOnTable();
  eaterRightFork = false;
  eaterLeftFork = false;
}
void BurgerEater::print() const
{
  std::cout << "\n"
            << "Eater no. " << id << " ate " << idOfEatenBurgers.size()
            << " burgers" << std::endl;
  /*          << " burgers with ids as follows:" << std::endl;
        for (const auto &i : idOfEatenBurgers_)
  {
          std::cout << "{" << i << "} ";
  };*/
  std::cout << "\n";
}