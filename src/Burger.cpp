#include "Burger.hpp"
#include "Fork.hpp"

#include <iostream>

Burger::Burger(int id) : id(id), isConsumed(false) {}

Burger::~Burger()
{
  mtx.unlock();
}

Burger::Burger(Burger &&burger)
{
  std::unique_lock<std::mutex> rhs_lk(burger.mtx);
  id = std::move(burger.id);
  //isConsumed = std::move(burger.isConsumed);
}
Burger &Burger::operator=(Burger &&rhs)
{
  if (this != &rhs)
  { // both rhs and lhs needs to be blocked to prevent other
    // thread accesing it during move operation
    std::unique_lock<std::mutex> lhs_lk(mtx, std::defer_lock);
    std::unique_lock<std::mutex> rhs_lk(rhs.mtx, std::defer_lock);
    std::lock(lhs_lk, rhs_lk);
    id = std::move(rhs.id);
    // isConsumed = std::move(rhs.isConsumed);
  }
  return *this;
}

bool Burger::tryConsume()
{
  bool locked = mtx.try_lock();

  if (locked && (isConsumed == false))
  {
    isConsumed = true;
    //mtx.unlock();
    return true;
  }
  else
  {
    // mtx.unlock();
    return false;
  }
}

int Burger::getID() const { return id; }
